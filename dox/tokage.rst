.. currentmodule:: tokage

Class Reference
===============

Client
-------

.. autoclass:: Client
    :members:

Base Classes
-------------
.. warning:: Do not create these yourself. You'll recieve them by way of getter functions.

Anime
~~~~~~
.. autoclass:: Anime

Manga
~~~~~~ 
.. autoclass:: Manga

Character
~~~~~~~~~~
.. autoclass:: Character

Person
~~~~~~~~~~
.. autoclass:: Person
