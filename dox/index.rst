.. Tokage documentation master file, created by
   sphinx-quickstart on Tue Sep 19 21:40:25 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tokage's documentation!
==================================

Tokage is an asyncio-based wr!apper for the MAL api (using jikan.me).
This module features an easy to understand object-oriented style.

Contents
--------

.. toctree::
    :maxdepth: 2

    getting_started
    tokage
    errors

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
