"""
Tokage is an async MAL wrapper for Python 3.5+.

.. currentmodule:: tokage

.. autosummary::
    :toctree: Tokage

    client
    abc
    errors
"""

from .client import Client
from .errors import *
from .abc import *
